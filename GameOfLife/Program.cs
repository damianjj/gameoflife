﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;

            Console.WriteLine("Podaj rozmiar planszy: ");
            string x = Console.ReadLine();

            Console.WriteLine("Podaj ilość elementów początkowych: ");
            string z = Console.ReadLine();

            int sizeOfBoard = int.Parse(x);
            int cells = int.Parse(z);

            Board board1 = new Board(sizeOfBoard);

            board1.CreateNewBoard();
            board1.SetCellOnBoard(cells);

            int end = 1;

            while(end == 1)
            {
                Console.Write("\n");
                Console.WriteLine("Iteracja numer " + i);
                i++;
                Console.Write("\n");
                board1.ShowBoard();
                board1.ResetNeighbours();
                board1.CellNeighbours();
                board1.IsAlive();
                end = board1.IsEnd();
                Console.Write("\n");
                Console.ReadLine();
            }

            Console.WriteLine("Koniec ! Wszystkie komórki umarły ! Zajęło to " + i + " iteracji !");
            Console.Read();
        }
    }
}
