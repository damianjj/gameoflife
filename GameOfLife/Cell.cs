﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Cell
    {
        public bool alive;
        public int neighbours;

        public Cell()
        {
            alive = false;
            neighbours = 0;
        }
    }
}
