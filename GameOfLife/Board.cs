﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Board
    {
        public Cell[,] board;
        public int boardSize;

        public Board(int n) // konstruktor
        {
            boardSize = n;
            board = new Cell[boardSize, boardSize];
        }

        public void CreateNewBoard() // tworzenie planszy
        {
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    board[i, j] = new Cell();
                }
            }
        }

        public void ShowBoard() // wyswietlanie planszy
        {
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    if (board[i, j].alive == true)
                    {
                        Console.Write("1    ");
                    }
                    else if (board[i, j].alive == false)
                    {
                        Console.Write("0    ");
                    }
                }
                Console.WriteLine("\n");
            }
        }

        public void SetCellOnBoard(int livingCells) // wypelnianie planszy komorkami
        {
            while (livingCells > 0)
            {
                Random rnd = new Random();
                int x = rnd.Next(0, boardSize);
                int y = rnd.Next(0, boardSize);

                if (board[x, y].alive == false)
                {
                    board[x, y].alive = true;
                    livingCells--;
                }

            }
        }

        public void CellNeighbours() // zliczanie sasiadow komorki
        {
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    if ((i == 0) && (j == 0)) // lewy gorny rog
                    {
                        for (int k = i; k <= i + 1; k++)
                        {
                            for (int l = j; l <= j + 1; l++)
                            {
                                if (board[k, l].alive == true)
                                {
                                    board[i, j].neighbours++;
                                }
                            }
                        }
                    }
                    else if (i == 0 && j == boardSize) // lewy dolny rog
                    {
                        for (int k = i; k <= i + 1; k++)
                        {
                            for (int l = j - 1; l < j + 1; l++)
                            {
                                if (board[k, l].alive == true)
                                {
                                    board[i, j].neighbours++;
                                }
                            }
                        }
                    }
                    else if (i == boardSize && j == 0) // prawy gorny rog
                    {
                        for (int k = i - 1; k < i + 1; k++)
                        {
                            for (int l = j; l <= j + 1; l++)
                            {
                                if (board[k, l].alive == true)
                                {
                                    board[i, j].neighbours++;
                                }
                            }
                        }
                    }
                    else if (i == boardSize && j == boardSize) // prawy dolny rog
                    {
                        for (int k = i - 1; k < i ; k++)
                        {
                            for (int l = j - 1; l < j ; l++)
                            {
                                if (board[k, l].alive == true)
                                {
                                    board[i, j].neighbours++;
                                }
                            }
                        }
                    }
                    else if (i > 0 && i < boardSize - 1 && j == 0) // gora planszy
                    {
                        for (int k = i - 1; k <= i + 1; k++)
                        {
                            for (int l = j; l <= j + 1; l++)
                            {
                                if (board[k, l].alive == true)
                                {
                                    board[i, j].neighbours++;
                                }
                            }
                        }
                    }
                    else if (i > 0 && i < boardSize - 1 && j == boardSize - 1) // dol planszy
                    {
                        for (int k = i - 1; k <= i + 1; k++)
                        {
                            for (int l = j - 1; l <= j; l++)
                            {
                                if (board[k, l].alive == true)
                                {
                                    board[i, j].neighbours++;
                                }
                            }
                        }
                    }
                    else if ((i == 0) && (j > 0) && (j < boardSize - 1)) // lewa strona planszy
                    {
                        for (int k = i; k <= i + 1; k++)
                        {
                            for (int l = j - 1; l <= j + 1; l++)
                            {
                                if (board[k, l].alive == true)
                                {
                                    board[i, j].neighbours++;
                                }
                            }
                        }
                    }
                    else if (i == boardSize - 1 && j > 0 && j < boardSize - 1) // prawa strona planszy
                    {
                        for (int k = i - 1; k <= i; k++)
                        {
                            for (int l = j - 1; l <= j + 1; l++)
                            {
                                if (board[k, l].alive == true)
                                {
                                    board[i, j].neighbours++;
                                }
                            }
                        }
                    }
                    else if ((i > 0) && (j > 0) && (i < boardSize - 1) && (j < boardSize - 1)) // srodek planszy
                    {
                        for (int k = i - 1; k <= i + 1; k++)
                        {
                            for (int l = j - 1; l <= j + 1; l++)
                            {
                                if (board[k, l].alive == true)
                                {
                                    board[i, j].neighbours++;
                                }
                            }
                        }
                    }

                    if (board[i, j].alive == true)
                    {
                        board[i, j].neighbours--;
                    }
                }
            }
        }

        public void ResetNeighbours() // reset zliczania sasiadow
        {
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    board[i, j].neighbours = 0;
                }
            }
        }

        public void IsAlive() // sprawdzanie zyjacych komorek
        {
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    if (board[i, j].alive == true && board[i, j].neighbours < 2) // mniej niz 2 sasiadow - umiera
                    {
                        board[i, j].alive = false;
                    }
                    else if (board[i, j].alive == true && board[i, j].neighbours > 3) // wiecej niz 3 sasiadow - umiera
                    {
                        board[i, j].alive = false;
                    }
                    else if (board[i, j].alive == false && board[i, j].neighbours == 3) // dokladnie 3 sasiadow - reprodukcja
                    {
                        board[i, j].alive = true;
                    }
                }
            }
        }

        public int IsEnd()
        {
            int cells = boardSize*boardSize;

            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    if (board[i, j].alive == false)
                    {
                        cells--;
                    } 
                }
            }

            if (cells == 0)
            {
                return 0;
            }

            return 1;
        }
    }
}
